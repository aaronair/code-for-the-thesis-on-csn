# Разрешение контекста цитирования с учетом внутреннего нарратива (Citation Source Narrowing, CSN)

В данном репозитории представлен код, написанный для практической части дипломной работы.

- [cites_abstract.ipynb](https://gitlab.com/aaronair/code-for-the-thesis-on-csn/-/blob/master/cites_abstract.ipynb) - ноутбук со сбором данных из [Microsoft Academic Knowledge Graph (MAKG)](http://ma-graph.org/) для первого исследования.
- [abstr-cites.csv](https://gitlab.com/aaronair/code-for-the-thesis-on-csn/-/blob/master/abstr-cites.csv) - файл с данными (абстрактами) генерируемый предыдущим кодом.
- [abstr_PSA.ipynb](https://gitlab.com/aaronair/code-for-the-thesis-on-csn/-/blob/master/abstr_PSA.ipynb) - первое исследование: применение PСA и t-SNE для отобранных абстрактов.
- [cite_wiki.ipynb](https://gitlab.com/aaronair/code-for-the-thesis-on-csn/-/blob/master/cite_wiki.ipynb) - пример работы простого базового алгоритма решения задачи CSN.
